import requests
import json

# URL der Emoji-Daten
url = 'https://gist.githubusercontent.com/oliveratgithub/0bf11a9aff0d6da7b46f1490f86a71eb/raw/d8e4b78cfe66862cf3809443c1dba017f37b61db/emojis.json'

# Daten herunterladen
response = requests.get(url)
data = response.json()

# Liste für die umstrukturierten Daten
new_data = []

# Durch die Originaldaten iterieren und umstrukturieren
for emoji in data['emojis']:
    category_parts = emoji['category'].split(' (')
    category = category_parts[0]
    if category == "":
        category = "Objects"
    if category == "(subdivision-flag)":
        category = "Flags"

    subcategory = category_parts[1].rstrip(')') if len(category_parts) > 1 else ''

    # Neues Emoji-Objekt erstellen
    new_emoji = {
        "emoji": emoji["emoji"],
        "name": emoji["name"],
        "shortname": emoji["shortname"],
        "unicode": emoji["unicode"],
        "html": emoji["html"],
        "category": category,
        "subcategory": subcategory,
        "order": emoji["order"]
    }
    new_data.append(new_emoji)

# Neue Daten in eine Datei schreiben
with open('emojis.json', 'w', encoding='utf-8') as f:
    json.dump(new_data, f, ensure_ascii=False, indent=4)

print("Data updated")

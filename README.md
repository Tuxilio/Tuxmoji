# Tuxmoji

Free and open source emoji search

[Use it »](https://tuxilio.codeberg.page/tuxmoji/)

## Credits

Emoji list based on <https://gist.github.com/oliveratgithub/0bf11a9aff0d6da7b46f1490f86a71eb>

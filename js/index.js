document.addEventListener('DOMContentLoaded', function () {
    const emojiContainer = document.getElementById('emojiContainer');
    const emojiModal = document.getElementById('emojiModal');
    const emojiDetails = document.getElementById('emojiDetails');
    const closeModal = document.getElementsByClassName("close")[0];
    let emojis = [];

    function loading() {
        emojiContainer.innerHTML = '';
        const div = document.createElement('div');
        div.textContent = "Loading...";
        emojiContainer.appendChild(div);
    }

    // Load emojis
    loading();
    fetch('emojis.json')
        .then(response => response.json())
        .then(data => {
            emojis = data;
            populateCategoryButtons(data);
            displayEmojis(data);
        })
        .catch(error => console.error('Fehler beim Laden der Emojis:', error));


    function displayEmojis(emojis) {
        emojiContainer.innerHTML = '';
        emojis.forEach(emoji => {
            const div = document.createElement('div');
            div.textContent = emoji.emoji;
            div.title = emoji.name;
            div.style.cursor = 'pointer';
            div.onclick = function() {
                navigator.clipboard.writeText(emoji.emoji).then(() => {
                    showModal(emoji);
                });
            };
            emojiContainer.appendChild(div);
        });
    }

    function populateCategoryButtons(data) {
        const categories = Array.from(new Set(data.map(emoji => emoji.category)));
        const buttonContainer = document.querySelector('.category-buttons');
        categories.sort();
        categories.unshift('All');

        categories.forEach(category => {
            const button = document.createElement('button');
            button.textContent = category;
            button.onclick = function() {
                document.querySelectorAll('.category-buttons button').forEach(btn => btn.classList.remove('active'));
                button.classList.add('active');
                filterEmojisByCategory(category);
            };
            buttonContainer.appendChild(button);
        });
        buttonContainer.children[0].classList.add('active');
    }

    function filterEmojisByCategory(category) {
        const filteredEmojis = category === 'All' ? emojis : emojis.filter(emoji => emoji.category === category);
        displayEmojis(filteredEmojis);
    }


    // modal
    function showModal(emoji) {
        emojiDetails.innerHTML = `<h1>${emoji.emoji}</h1> Name: <code>${emoji.name}</code> <br><br> Shortname: <code>${emoji.shortname}</code> <br><br> Unicode Code: <code>${emoji.unicode}</code> <br><br> HTML code: <code>`;
        const div = document.createElement('div');
        div.textContent = emoji.html;
        emojiDetails.appendChild(div);
        emojiModal.style.display = "block";
    }
    closeModal.onclick = function() {
        emojiModal.style.display = "none";
    };
    window.onclick = function(event) {
        if (event.target == emojiModal) {
            emojiModal.style.display = "none";
        }
    };

    // Search function
    function filterEmojis() {
        const search = document.getElementById('searchBox').value.toLowerCase();
        const filteredEmojis = emojis.filter(emoji =>
            emoji.name.toLowerCase().includes(search) ||
            emoji.shortname.includes(search) ||
            emoji.unicode.toLowerCase().includes(search)
        );
        displayEmojis(filteredEmojis);
    }

    document.getElementById('searchBox').onkeyup = filterEmojis;
});
